#ifndef WIN_H
#define WIN_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class win; }
QT_END_NAMESPACE

class win : public QMainWindow
{
    Q_OBJECT

public:
    win(QWidget *parent = nullptr);
    ~win();

private slots:
    void on_surf_back_pressed();

    void on_surf_webview_loadStarted();

    void on_surf_next_pressed();

private:
    Ui::win *ui;
};
#endif // WIN_H
