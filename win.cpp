﻿#include "win.h"
#include "ui_win.h"

#include <json.hpp>
using json = nlohmann::json;
//Lohmann, N. JSON for Modern C++ (Version 3.10.5) [Computer software]. https://github.com/nlohmann
#include <curl/curl.h>

#include <iostream>
using namespace std;

json webring_json;
vector < string > sites_vector;
int posinring = 0;

size_t write_funcgr(void * ptr, size_t size, size_t nmemb, std::string * s) {
  s -> append(static_cast < char * > (ptr), size * nmemb);
  return size * nmemb;
}
win::win(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::win)
{
//Fetch Webring
    CURL * curl;
    cout << "Fetching Webring...\n";
    curl = curl_easy_init();
    if (curl) {
        string s;
        ostringstream stream;
        curl_easy_setopt(curl, CURLOPT_URL, "https://miau.sadgrl.online/yesterweb-ring/webring.json");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_funcgr);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, & s);
        curl_easy_perform(curl);
        webring_json = json::parse(s);
        curl_easy_cleanup(curl);
        for (json & i: webring_json) {
            string current_wrs_url = i.value("url", "").c_str();
            sites_vector.push_back((*current_wrs_url.rbegin()=='/') ? current_wrs_url : current_wrs_url + "/");
        }
    }
    ui->setupUi(this);
    ui->surf_webview->setUrl(QUrl("http://yesterweb.org/"));
    ui->surf_webview->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled,true);
    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->toolBar->insertWidget(ui->actionForward, spacer);
}

win::~win()
{
    delete ui;
}



void win::on_surf_webview_loadStarted()
{
    string website = ui->surf_webview->url().toString().toStdString();
    int posfor = 0;
    for (auto i: sites_vector) {
        if (i==website) {
            posinring=posfor;
            //ui->surf_ringpos->setValue(posinring);
            break;
        } else {
            posfor++;
        }

    }
}

void win::on_surf_back_pressed()
{
    if (posinring==0) {
        ui->surf_webview->setUrl(QUrl(sites_vector[sites_vector.size()-1].c_str()));
    } else {
        ui->surf_webview->setUrl(QUrl(sites_vector[posinring-1].c_str()));
    }
}
void win::on_surf_next_pressed()
{
    if (posinring==sites_vector.size()-1){
        ui->surf_webview->setUrl(QUrl(sites_vector[0].c_str()));
    } else {
        ui->surf_webview->setUrl(QUrl(sites_vector[posinring+1].c_str()));
    }
}


