/********************************************************************************
** Form generated from reading UI file 'win.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIN_H
#define UI_WIN_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_win
{
public:
    QAction *actionBack;
    QAction *actionForward;
    QAction *actionAbout;
    QAction *actionQuit;
    QAction *actionBack_2;
    QAction *actionForward_2;
    QAction *actionBack_3;
    QAction *actionForward_3;
    QAction *actionGoto_URL;
    QAction *actionPrevious;
    QAction *actionNext;
    QAction *actionGoto_ID;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QTabWidget *wintabs;
    QWidget *surf;
    QGridLayout *gridLayout_2;
    QFrame *frame;
    QVBoxLayout *surf_vertical;
    QWebView *surf_webview;
    QHBoxLayout *surf_surfer;
    QPushButton *surf_back;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *surf_next;
    QWidget *radio;
    QGridLayout *gridLayout_3;
    QWebView *webView;
    QToolBar *toolBar;

    void setupUi(QMainWindow *win)
    {
        if (win->objectName().isEmpty())
            win->setObjectName(QString::fromUtf8("win"));
        win->resize(1160, 674);
        actionBack = new QAction(win);
        actionBack->setObjectName(QString::fromUtf8("actionBack"));
        actionBack->setEnabled(true);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("back");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionBack->setIcon(icon);
        actionForward = new QAction(win);
        actionForward->setObjectName(QString::fromUtf8("actionForward"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("next");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionForward->setIcon(icon1);
        actionAbout = new QAction(win);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionQuit = new QAction(win);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionBack_2 = new QAction(win);
        actionBack_2->setObjectName(QString::fromUtf8("actionBack_2"));
        actionForward_2 = new QAction(win);
        actionForward_2->setObjectName(QString::fromUtf8("actionForward_2"));
        actionBack_3 = new QAction(win);
        actionBack_3->setObjectName(QString::fromUtf8("actionBack_3"));
        actionForward_3 = new QAction(win);
        actionForward_3->setObjectName(QString::fromUtf8("actionForward_3"));
        actionGoto_URL = new QAction(win);
        actionGoto_URL->setObjectName(QString::fromUtf8("actionGoto_URL"));
        actionPrevious = new QAction(win);
        actionPrevious->setObjectName(QString::fromUtf8("actionPrevious"));
        actionNext = new QAction(win);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        actionGoto_ID = new QAction(win);
        actionGoto_ID->setObjectName(QString::fromUtf8("actionGoto_ID"));
        centralwidget = new QWidget(win);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        wintabs = new QTabWidget(centralwidget);
        wintabs->setObjectName(QString::fromUtf8("wintabs"));
        wintabs->setTabPosition(QTabWidget::West);
        wintabs->setTabShape(QTabWidget::Triangular);
        surf = new QWidget();
        surf->setObjectName(QString::fromUtf8("surf"));
        gridLayout_2 = new QGridLayout(surf);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(surf);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMouseTracking(true);
        surf_vertical = new QVBoxLayout(frame);
        surf_vertical->setSpacing(0);
        surf_vertical->setObjectName(QString::fromUtf8("surf_vertical"));
        surf_vertical->setContentsMargins(0, 0, 0, 0);
        surf_webview = new QWebView(frame);
        surf_webview->setObjectName(QString::fromUtf8("surf_webview"));
        surf_webview->setUrl(QUrl(QString::fromUtf8("https://tilde.club/~roly/")));
        surf_webview->setRenderHints(QPainter::SmoothPixmapTransform);

        surf_vertical->addWidget(surf_webview);

        surf_surfer = new QHBoxLayout();
        surf_surfer->setObjectName(QString::fromUtf8("surf_surfer"));
        surf_surfer->setSizeConstraint(QLayout::SetDefaultConstraint);
        surf_surfer->setContentsMargins(0, -1, -1, -1);
        surf_back = new QPushButton(frame);
        surf_back->setObjectName(QString::fromUtf8("surf_back"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(surf_back->sizePolicy().hasHeightForWidth());
        surf_back->setSizePolicy(sizePolicy);
        surf_back->setAutoFillBackground(false);
        surf_back->setIcon(icon);
        surf_back->setCheckable(false);
        surf_back->setAutoRepeat(false);
        surf_back->setAutoDefault(false);
        surf_back->setFlat(false);

        surf_surfer->addWidget(surf_back);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        surf_surfer->addItem(horizontalSpacer_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        surf_surfer->addItem(horizontalSpacer);

        surf_next = new QPushButton(frame);
        surf_next->setObjectName(QString::fromUtf8("surf_next"));
        sizePolicy.setHeightForWidth(surf_next->sizePolicy().hasHeightForWidth());
        surf_next->setSizePolicy(sizePolicy);
        surf_next->setAcceptDrops(false);
        surf_next->setIcon(icon1);

        surf_surfer->addWidget(surf_next);


        surf_vertical->addLayout(surf_surfer);


        gridLayout_2->addWidget(frame, 0, 0, 1, 1);

        wintabs->addTab(surf, QString());
        radio = new QWidget();
        radio->setObjectName(QString::fromUtf8("radio"));
        gridLayout_3 = new QGridLayout(radio);
        gridLayout_3->setSpacing(0);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        webView = new QWebView(radio);
        webView->setObjectName(QString::fromUtf8("webView"));
        webView->setUrl(QUrl(QString::fromUtf8("qrc:/radio.html")));

        gridLayout_3->addWidget(webView, 0, 0, 1, 1);

        wintabs->addTab(radio, QString());

        gridLayout->addWidget(wintabs, 0, 0, 1, 1);

        win->setCentralWidget(centralwidget);
        toolBar = new QToolBar(win);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setEnabled(true);
        toolBar->setContextMenuPolicy(Qt::PreventContextMenu);
        toolBar->setMovable(true);
        toolBar->setOrientation(Qt::Horizontal);
        toolBar->setFloatable(false);
        win->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addAction(actionBack);
        toolBar->addSeparator();
        toolBar->addAction(actionForward);

        retranslateUi(win);

        wintabs->setCurrentIndex(1);
        surf_back->setDefault(false);
        surf_next->setDefault(false);


        QMetaObject::connectSlotsByName(win);
    } // setupUi

    void retranslateUi(QMainWindow *win)
    {
        win->setWindowTitle(QCoreApplication::translate("win", "win", nullptr));
        actionBack->setText(QCoreApplication::translate("win", "Back", nullptr));
#if QT_CONFIG(tooltip)
        actionBack->setToolTip(QCoreApplication::translate("win", "Go Back", nullptr));
#endif // QT_CONFIG(tooltip)
        actionForward->setText(QCoreApplication::translate("win", "Forward", nullptr));
#if QT_CONFIG(tooltip)
        actionForward->setToolTip(QCoreApplication::translate("win", "Go", nullptr));
#endif // QT_CONFIG(tooltip)
        actionAbout->setText(QCoreApplication::translate("win", "About", nullptr));
        actionQuit->setText(QCoreApplication::translate("win", "Quit", nullptr));
        actionBack_2->setText(QCoreApplication::translate("win", "Back", nullptr));
        actionForward_2->setText(QCoreApplication::translate("win", "Forward", nullptr));
        actionBack_3->setText(QCoreApplication::translate("win", "Back", nullptr));
        actionForward_3->setText(QCoreApplication::translate("win", "Forward", nullptr));
        actionGoto_URL->setText(QCoreApplication::translate("win", "Goto URL", nullptr));
        actionPrevious->setText(QCoreApplication::translate("win", "Previous", nullptr));
        actionNext->setText(QCoreApplication::translate("win", "Next", nullptr));
        actionGoto_ID->setText(QCoreApplication::translate("win", "Goto ID", nullptr));
#if QT_CONFIG(tooltip)
        surf_back->setToolTip(QCoreApplication::translate("win", "Go back in the webring", nullptr));
#endif // QT_CONFIG(tooltip)
        surf_back->setText(QCoreApplication::translate("win", "Previous", nullptr));
#if QT_CONFIG(tooltip)
        surf_next->setToolTip(QCoreApplication::translate("win", "Go forward in the webring", nullptr));
#endif // QT_CONFIG(tooltip)
        surf_next->setText(QCoreApplication::translate("win", "Next", nullptr));
        wintabs->setTabText(wintabs->indexOf(surf), QCoreApplication::translate("win", "Surf", nullptr));
        wintabs->setTabText(wintabs->indexOf(radio), QCoreApplication::translate("win", "Radio", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("win", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class win: public Ui_win {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIN_H
